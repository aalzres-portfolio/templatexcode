//___FILEHEADER___

import Foundation

protocol ___VARIABLE_moduleName___PresenterOutput: class {}

protocol ___VARIABLE_moduleName___Presenter {}

class ___VARIABLE_moduleName___PresenterImpl: ___VARIABLE_moduleName___Presenter {
    private let router: ___VARIABLE_moduleName___Router
    private let interactor: ___VARIABLE_moduleName___
    
    weak var output: ___VARIABLE_moduleName___PresenterOutput?
    
    init(router: ___VARIABLE_moduleName___Router, interactor: ___VARIABLE_moduleName___) {
        self.interactor = interactor
        self.router = router
    }
}

extension ___VARIABLE_moduleName___PresenterImpl: ___VARIABLE_moduleName___Output {}
