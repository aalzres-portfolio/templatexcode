//___FILEHEADER___

import UIKit

class ___VARIABLE_moduleName___VC: UIViewController {
    private let presenter: ___VARIABLE_moduleName___Presenter
    
    init(presenter: ___VARIABLE_moduleName___Presenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    //MARK: - Setups
    private func setupView() {
        view.backgroundColor = .white
    }
}

extension ___VARIABLE_moduleName___VC: ___VARIABLE_moduleName___PresenterOutput {}
