//___FILEHEADER___

import Foundation

protocol ___VARIABLE_moduleName___RepositoryOutput: class {}

protocol ___VARIABLE_moduleName___Repository {}

class ___VARIABLE_moduleName___RepositoryImpl: ___VARIABLE_moduleName___Repository {
    weak var output: ___VARIABLE_moduleName___RepositoryOutput?
}
