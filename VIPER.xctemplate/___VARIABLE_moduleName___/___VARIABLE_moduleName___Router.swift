//___FILEHEADER___

import UIKit

class ___VARIABLE_moduleName___Router {
    class func create() -> ___VARIABLE_moduleName___VC {
        let repository = ___VARIABLE_moduleName___RepositoryImpl()
        let router = ___VARIABLE_moduleName___Router()
        let interactor = ___VARIABLE_moduleName___Impl(repository: repository)
        let presenter = ___VARIABLE_moduleName___PresenterImpl(router: router, interactor: interactor)
        repository.output = interactor
        interactor.output = presenter
        let vc = ___VARIABLE_moduleName___VC(presenter: presenter)
        presenter.output = vc
        
        return vc
    }
}
