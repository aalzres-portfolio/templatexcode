//___FILEHEADER___

import Foundation

protocol ___VARIABLE_moduleName___Output: class {}

protocol ___VARIABLE_moduleName___ {}

class ___VARIABLE_moduleName___Impl: ___VARIABLE_moduleName___ {
    private let repository: ___VARIABLE_moduleName___Repository
    
    weak var output: ___VARIABLE_moduleName___Output?
    
    init(repository: ___VARIABLE_moduleName___Repository) {
        self.repository = repository
    }
}

extension ___VARIABLE_moduleName___Impl: ___VARIABLE_moduleName___RepositoryOutput {}
